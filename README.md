![Universis](src/assets/img/universis_logo_128_color.png)
## @universis/ngx-tables

An implementation of [datatables.net](https://github.com/DataTables/DataTables) for [Universis project](https://gitlab.com/universis) applications.

## Installation

Install @universis/ngx-tables in an angular cli by executing:

    npm i @universis/ngx-tables

Important note: Don't forget to install peer dependencies also.

## Usage

Import `TablesModule`:

    # app.module.ts
    import { TablesModule } from '@universis/ngx-tables';
    ...
    imports: [
      CommonModule,
      BrowserModule,
      HttpClientModule,
      TranslateModule.forRoot(),
      SharedModule.forRoot(),
      RouterModule,
      ...
      ...
      TablesModule
    ],

Add `AdvancedTableComponent`:

    <app-advanced-table #table [config]="tableConfiguration"></app-advanced-table>

and load a table configuration:

    export const TABLE1_COFIGURATION = {
      "title": "Students",
      "model": "Students",
      "searchExpression":"indexof(person/familyName, '${text}') ge 0 or indexof(person/givenName, '${text}') ge 0",
      "selectable": false,
      "multipleSelect": false,
      "columns": [
        {
          "name":"id",
          "property": "id",
          "hidden": true
        },
        {
          "name":"person/familyName",
          "property": "familyName",
          "title":"Family Name"
        },
        {
          "name":"person/givenName",
          "property": "givenName",
          "title":"Given Name"
        },
        ...
      ],
      "criteria": [
        {
          "name": "studentName",
          "filter": "(indexof(person/familyName, '${value}') ge 0 or indexof(person/givenName, '${value}') ge 0)",
          "type": "text"
        },
        {
          "name": "username",
          "filter": "(indexof(user/name, '${value}') ge 0)",
          "type": "text"
        },
        ...
      ],
      "searches": [
      ],
      "defaults":{
        "filter": "studentStatus/alternateName eq eq 'active'"
      },
      "paths" : [
      ]
    }

Use this configuration in component:

    # table1.component.ts
    import {TABLE1_COFIGURATION} from './table1.configuration';
    @Component({
      selector: 'app-table1',
      templateUrl: './table1.component.html'
    })
    export class Table1Component implements OnInit {
      
      public tableConfiguration: any = TABLE1_COFIGURATION;
      public filter: any = {};
      
      constructor() {
      }

      ngOnInit() {
      }

    }

`AdvancedTableComponent` uses table configuration to load data from the given `model` and draw datatable.
Use `AdvancedTableComponent.configSrc` to load table configuration from a json file.

    <app-advanced-table #table [autoLoad]=false [configSrc]="'/assets/tables/table3.config.json'"></app-advanced-table>

`AdvancedSearchComponent` enables search operation against a loaded datatable.

    <app-advanced-table-search #advancedSearch [mergeQueryParams]="true" [(filter)]="filter.value" [collapsed]="false" [showMore]="true" [table]="table">
     <advanced-search>
          <app-advanced-search-form [formSrc]="'/assets/tables/table2.search.json'" #search [(filter)]="filter.value"></app-advanced-search-form>
      </advanced-search>
    </app-advanced-table-search>

`AdvancedSearchComponent` has a simple text box for text search based on `AdvancedTableComponent#config.searchExpression` expression.

`AdvancedSearchComponent` may also have an advanced search form for searching by using lookup tables, numbers, booleans etc.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run ng build tables` to build the project. The build artifacts will be stored in the `dist/tables` directory. Use the `--prod` flag for a production build.


