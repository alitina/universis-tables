import { Component, OnInit, ViewChild, Output, EventEmitter, OnDestroy, ViewEncapsulation } from '@angular/core';
import { ModalService, ErrorService } from '@universis/common';
import { Observable } from 'rxjs';
import { AdvancedTableComponent, AdvancedRowActionComponent, TableConfiguration, AdvancedSelectService, AdvancedTableEditorDirective } from 'projects/ngx-tables/src/public_api';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-table6',
  templateUrl: './table6.component.html',
  styles: [
    `
    .btn.disabled, .btn:disabled {
      background-color: inherit !important;
      border: none;
    }
    `
  ]
})
export class Table6Component implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    if (this.configSubscription) {
      this.configSubscription.unsubscribe();
    }
  }

  @ViewChild(AdvancedTableEditorDirective) tableEditor: AdvancedTableEditorDirective;
  @ViewChild('table') table: AdvancedTableComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  private configSubscription: any;
  public filter: any;

  constructor(private _modalService: ModalService,
    private _context: AngularDataContext,
    private _errorService: ErrorService,
    private _selectService: AdvancedSelectService) { }

  ngOnInit() {
    this.configSubscription = this.table.configChanges.subscribe((tableConfiguration: TableConfiguration) => {
      if (tableConfiguration) {
        const select = tableConfiguration.columns.filter( column => {
            if ( column.virtual === true ) {
              return false;
            }
            return true;
          }).map(column => {
            if (column.property) {
              return column.name + ' as ' + column.property;
            }
            return column.name;
          });
          this._context.model('Students').select(...select).where('semester').equal(4).getItems().then((items) => {
            this.tableEditor.set(items);
          });
      }
    });
  }

  apply() {
    // get items
    const items = this.tableEditor.rows();
    // place your code here
  }

  add() {
    this._selectService.select({
        modalTitle: 'Select student',
        tableConfigSrc: '/assets/tables/table4.config.json'
      }).then((result) => {
        if (result.result === 'ok') {
          this.tableEditor.add(...result.items);
        }
      });
  }

  remove() {
    try {
      const selected = this.table.selected;
      if (selected.length === 0) {
        return;
      }
      this.tableEditor.remove(...selected);
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

}
