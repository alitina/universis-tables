import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './layouts/index.component';
import { AuthGuard } from '@universis/common';
import { Table1Component } from './table1/table1.component';
import { Table2Component } from './table2/table2.component';
import { Table3Component } from './table3/table3.component';
import { AdvancedListComponent, AdvancedTableConfigurationResolver, AdvancedSearchConfigurationResolver } from 'projects/ngx-tables/src/public_api';
import { Select1Component } from './select1/select1.component';
import { Table4Component } from './table4/table4.component';
import { Table5Component } from './table5/table5.component';
import { Table6Component } from './table6/table6.component';
import { Table7Component } from './table7/table7.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'tables',
    component: IndexComponent,
    canActivateChild: [
      AuthGuard
    ],
    children: [
       {
        path: 'select1',
        component: Select1Component,
        pathMatch: 'full',
      },
      {
        path: 'table1',
        component: Table1Component,
        pathMatch: 'full',
      },
      {
        path: 'table2',
        component: Table2Component,
        pathMatch: 'full',
      },
      {
        path: 'table3',
        component: Table3Component,
        pathMatch: 'full',
      },
      {
        path: 'table4',
        component: Table4Component,
        pathMatch: 'full',
      },
      {
        path: 'table5',
        component: Table5Component,
        pathMatch: 'full',
      },
      {
        path: 'table6',
        component: Table6Component,
        pathMatch: 'full',
      },
      {
        path: 'table7',
        component: Table7Component,
        pathMatch: 'full',
      },
      {
        path: 'configuration/departments',
        component: AdvancedListComponent,
        pathMatch: 'full',
        data: {
          model: 'Departments',
          list: 'list',
          description: 'Departments.Description',
          longDescription: 'Departments.LongDescription'
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        }
      },
      {
        path: 'configuration/studyPrograms',
        component: AdvancedListComponent,
        pathMatch: 'full',
        data: {
          model: 'StudyPrograms',
          list: 'list',
          description: 'StudyPrograms.Description',
          longDescription: 'StudyPrograms.LongDescription'
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver,
          searchConfiguration: AdvancedSearchConfigurationResolver
        }
      }
    ]
  },
  {
    path: '',
    component: IndexComponent,
    canActivateChild: [
      AuthGuard
    ],
    children: [
      {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
