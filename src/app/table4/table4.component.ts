import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ModalService, ErrorService } from '@universis/common';
import { Observable } from 'rxjs';
import { AdvancedTableComponent, AdvancedRowActionComponent } from 'projects/ngx-tables/src/public_api';

@Component({
  selector: 'app-table4',
  templateUrl: './table4.component.html'
})
export class Table4Component implements OnInit {

  @ViewChild('table') table: AdvancedTableComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  constructor(private _modalService: ModalService,
    private _errorService: ErrorService) { }

  ngOnInit() {
  }


  executeAction1() {
    return new Observable((observer) => {
      const total = this.table.selected.length;
      const result = {
        total: this.table.selected.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        let index = 0;
        for (let item of this.table.selected) {
          try {
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            await new Promise((resolve) => {
              // do something
              setTimeout(() => {
                resolve();
              }, 1000);
            });
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
            index += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  action1() {
    try {
      const items = this.table.selected;
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: items, // set items
          modalTitle: 'Table action', // set title
          description: 'This action is going to be executed against each item of the selected items of advanced table', // set description
          refresh: this.refreshAction, // refresh action event
          execute: this.executeAction1()
        }
      });
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  action2() {
    try {
      const items = this.table.selected;
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: items, // set items
          modalTitle: 'Table action', // set title
          okButtonText: 'Execute action',
          okButtonClass: 'btn btn-success',
          progressType: 'success',
          description: 'This action is going to be executed against each item of the selected items of advanced table', // set description
          refresh: this.refreshAction, // refresh action event
          execute: this.executeAction1()
        }
      });
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

}
