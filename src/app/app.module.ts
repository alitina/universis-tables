import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, Injector } from '@angular/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {
  SharedModule, AuthModule, ErrorModule, ConfigurationService,
  APP_LOCATIONS, SIDEBAR_LOCATIONS, UserStorageService, LocalUserStorageService
} from '@universis/common';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap/modal';
import { IndexComponent } from './layouts/index.component';
import { LocationStrategy, HashLocationStrategy, registerLocaleData, CommonModule } from '@angular/common';
import { AngularDataContext, ClientDataContextConfig, DATA_CONTEXT_CONFIG, MostModule } from '@themost/angular';
import { AppSidebarModule } from '@coreui/angular';
import { environment } from '../environments/environment';
import * as fromTables from '@universis/ngx-tables';

// LOCALES: import extra locales here
import en from '@angular/common/locales/en';
import { THIS_APP_LOCATIONS } from './app.locations';
import { APP_SIDEBAR_LOCATIONS } from './app.sidebar.locations';
import { TablesModule } from 'projects/ngx-tables/src/public_api';
import { Table1Component } from './table1/table1.component';
import { Table2Component } from './table2/table2.component';
import { Table3Component } from './table3/table3.component';
import { Select1Component } from './select1/select1.component';
import { Table4Component } from './table4/table4.component';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { Table5Component } from './table5/table5.component';
import { Table6Component } from './table6/table6.component';
import { Table7Component } from './table7/table7.component';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    Table1Component,
    Table2Component,
    Table3Component,
    Select1Component,
    Table4Component,
    Table5Component,
    Table6Component,
    Table7Component
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    MostModule.forRoot({
      base: '/',
      options: {
        useMediaTypeExtensions: false,
        useResponseConversion: true,
      },
    }),
    SharedModule.forRoot(),
    RouterModule,
    AuthModule,
    FormsModule,
    AppRoutingModule,
    AppSidebarModule,
    ErrorModule.forRoot(),
    BsDropdownModule.forRoot(),
    ProgressbarModule.forRoot(),
    ModalModule.forRoot(),
    TablesModule,
    StoreModule.forRoot(reducers, { metaReducers }),
     StoreModule.forFeature(fromTables.featureName, fromTables.tableReducers)
    ],
  providers: [
    Title,
    {
      provide: APP_LOCATIONS,
      useValue: THIS_APP_LOCATIONS,
    },
    {
      provide: SIDEBAR_LOCATIONS,
      useValue: APP_SIDEBAR_LOCATIONS,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: (configurationService: ConfigurationService, context: AngularDataContext) =>
        async () => {
          await configurationService.load();
          registerLocaleData('en');
          registerLocaleData('el');
          context.setBase(configurationService.settings.remote.server);
        },
      deps: [ConfigurationService, AngularDataContext],
      multi: true,
    },
    {
      provide: LOCALE_ID,
      useFactory: (configurationService: ConfigurationService) => {
        return configurationService.currentLocale;
      },
      deps: [ConfigurationService],
    },
    // use hash location stategy
    // https://angular.io/api/common/HashLocationStrategy
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
    {
      provide: UserStorageService,
      useClass: LocalUserStorageService,
    },
  ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ],
})
export class AppModule {
  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch((err) => {
      console.error('An error occurred while init application module.');
      console.error(err);
    });
  }
  // tslint:disable-next-line: use-lifecycle-interface
  private async ngOnInit() {
    // create promises chain
    const sources = environment.languages.map(async (language) => {
      const translations = await import(`../assets/i18n/${language}.json`);
      this._translateService.setTranslation(language, translations, true);
    });
    // execute chain
    await Promise.all(sources);
  }
}
