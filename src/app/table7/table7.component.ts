import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AdvancedTableComponent, ActivatedTableService } from 'projects/ngx-tables/src/public_api';

@Component({
  selector: 'app-table7',
  templateUrl: './table7.component.html',
  styles: [`
    .text-exclamation:before {
      content: '! ';
    }
  `],
  encapsulation: ViewEncapsulation.None
})
export class Table7Component implements OnInit {

  @ViewChild('table') table: AdvancedTableComponent;
  constructor(private _activatedTableService: ActivatedTableService) { }
  public filter: any = {};

  ngOnInit() {
    this._activatedTableService.activeTable = this.table;
  }

}
