export const APP_SIDEBAR_LOCATIONS = [
  {
    name: 'Set table configuration',
    key: 'Sidebar.Table1',
    url: '/tables/table1',
    index: 10
  },
  {
    name: 'Load configuration from URL',
    key: 'Sidebar.Table2',
    url: '/tables/table2',
    index: 15
  },
  {
    name: 'Use advanced search',
    key: 'Sidebar.Table3',
    url: '/tables/table3',
    index: 10
  },
  {
    name: 'Use advanced list',
    key: 'Sidebar.StudyPrograms',
    url: '/tables/configuration/studyPrograms',
    index: 10
  },
  {
    name: 'Use select modal',
    key: 'Sidebar.Select1',
    url: '/tables/select1',
    index: 20
  },
  {
    name: 'Use row action',
    key: 'Sidebar.Table4',
    url: '/tables/table4',
    index: 20
  },
  {
    name: 'Use row action with custom form',
    key: 'Sidebar.Table5',
    url: '/tables/table5',
    index: 20
  },
  {
    name: 'Use table editor',
    key: 'Sidebar.Table6',
    url: '/tables/table6',
    index: 30
  },
  {
    name: 'Show aggregated data',
    key: 'Sidebar.Table7',
    url: '/tables/table7',
    index: 40
  }
];
