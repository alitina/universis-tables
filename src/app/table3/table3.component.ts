import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { AdvancedTableComponent, AdvancedTableSearchComponent } from '@universis/ngx-tables';
import { AppState } from '../reducers';

@Component({
  selector: 'app-table3',
  templateUrl: './table3.component.html',
  styles: []
})
export class Table3Component implements OnInit, AfterViewInit, OnDestroy {

  public filter: any = {};
  @ViewChild('search') search: AdvancedTableSearchComponent;
  @ViewChild('table') table: AdvancedTableComponent;
  private configChangesSubscription: any;

  constructor(private store: Store<AppState>) { }
  ngOnDestroy(): void {
    if (this.configChangesSubscription) {
      this.configChangesSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    
  }

  ngOnInit() {
   //
  }

}
