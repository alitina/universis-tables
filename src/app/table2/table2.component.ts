import { Component, OnInit, ViewChild } from '@angular/core';
import { AdvancedTableComponent, ActivatedTableService } from 'projects/ngx-tables/src/public_api';

@Component({
  selector: 'app-table2',
  templateUrl: './table2.component.html',
  styles: []
})
export class Table2Component implements OnInit {

  @ViewChild('table') table: AdvancedTableComponent;
  constructor(private _activatedTableService: ActivatedTableService) { }
  public filter: any = {};

  ngOnInit() {
    this._activatedTableService.activeTable = this.table;
  }

  addItem() {
    alert('Default action clicked');
  }

}
