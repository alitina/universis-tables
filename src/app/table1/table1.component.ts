import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as fromRoot from '../reducers';
import { AppState } from '../reducers';
import {TABLE1_COFIGURATION} from './table1.configuration';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { AdvancedTableSearchComponent } from '@universis/ngx-tables';
@Component({
  selector: 'app-table1',
  templateUrl: './table1.component.html'
})
export class Table1Component implements OnInit, AfterViewInit {

  public tableConfiguration: any = TABLE1_COFIGURATION;
  public filter: any = {};
  @ViewChild('search') search: AdvancedTableSearchComponent;

  constructor(private store: Store<AppState>) { }
  ngAfterViewInit(): void {
    //
  }

  ngOnInit() {
    //
  }

}

